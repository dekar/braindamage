﻿#!/bin/bash

platform=$(uname -o)
sdl_dir="/usr/include/SDL2"

if echo $platform | grep -i -q CYGWIN
then 
    sdl_dir="/cygdrive/c/MinGW/include/SDL2/"
elif echo $platform | grep -i -q MINGW
then
    sdl_dir="/include/SDL2/"
else
    echo "Unknown system type. Write deleloper to email)"
    exit 1
fi

function generate_tags_dir {
    ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .
    size=$(du -h tags)
    echo "Tag file size $size"
}

echo "Generating local tags"
pushd .
generate_tags_dir
popd
echo "DONE"

echo "Generating lua tags"
pushd /cygdrive/c/MinGW/include
files=$(find . -maxdepth 1 -name "*lua*")
ctags --append=1 -R --c++-kinds=+p --fields=+iaS --extra=+q $files
size=$(du -h tags)
echo "Tag file size $size"
popd
echo "DONE"

echo "Generating SDL tags"
pushd $sdl_dir
generate_tags_dir
popd
echo "DONE"
