﻿#include "app.h"

#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//There is header file confusion between mingw(windows platform)
//and Linux(posix platform)
#if defined(BD_WIN)
#include <SDL2/SDL_ttf.h>
#else
#include <SDL/SDL_ttf.h>
#endif

#include "engine.h"

fps_counter_t g_fps;

uint32_t fps_cb(uint32_t interval, void* p) {
    g_fps.value = g_fps.counter;
    g_fps.counter = 0;
    return 1000;
}

void sys_init(SDL_Window** window, SDL_Renderer** render) {

    srand(time(0));

    SDL_Init(SDL_INIT_EVERYTHING);
    atexit(SDL_Quit);

    SDL_LogSetAllPriority(SDL_LOG_PRIORITY_VERBOSE);

    TTF_Init();

    *window = SDL_CreateWindow(
            "braindamage",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            800,
            600,
            0);

    if (!*window) {
        LOG("Could not create window: %s\n", SDL_GetError());
        abort();
    }

    *render = SDL_CreateRenderer(*window, -1, SDL_RENDERER_TARGETTEXTURE);

    if (!*render) {
        LOG("Could not create render: %s\n", SDL_GetError());
        abort();
    }

    if (!SDL_RenderTargetSupported(*render)) {
        LOG("Rendering target are not supported. Sorry!\n");
        abort();
    }

#ifdef DEBUG
    SDL_BlendMode bm;
    int err = SDL_GetRenderDrawBlendMode(*render, &bm);
    assert(err == 0);
    LOG("render blend mode %d\n", bm);
#endif

    SDL_RenderSetLogicalSize(*render, 800, 600);
    SDL_SetRenderDrawBlendMode(*render, SDL_BLENDMODE_BLEND);
}

int main_loop(void (*entryPoint)(void)) {
    assert(entryPoint);

    SDL_Window*     window;
    SDL_Renderer*   render;
    bool quit = false;
    
    g_fps.counter = 0;
    g_fps.value = 0;
   
    sys_init(&window, &render);
    text_init();
    entryPoint();
    scn_init_manager(window);

    SDL_TimerID fps_timer = SDL_AddTimer(1000, fps_cb, NULL);

#ifdef DEBUG
    text_load_font("dejavu.ttf", "dejavu24", 24);
#endif

    while (!quit) {
        SDL_Event e;
        while (SDL_PollEvent(&e)) {
            switch (e.type) {
                case SDL_QUIT:
                    quit = true;
                break;
                case SDL_KEYDOWN:
                    if (e.key.keysym.sym == SDLK_F11) {
                        make_screen_shot(render);
                    }
                break;
            }
            scn_poll(&e);
        }

        scn_update();

        //Rendering stuff
        SDL_SetRenderDrawColor(render, 200, 200, 200, 255);
        SDL_RenderClear(render);
        scn_render(render);
#ifdef DEBUG
        SDL_Color c;
        c.r = 0;
        c.g = 0;
        c.b = 0;
        c.a = 255;
        text_set_color(c);
        text_set_active_font("dejavu24");
        text_set_align(ALIGN_LEFT);
        text_renderf(render, make_rect(0, 0, 0, 0), "FPS: %d", g_fps.value);
#endif
        SDL_RenderPresent(render);
        //End of rendering stuff

        SDL_Delay(3);

        g_fps.counter++;
    }

    LOG("end of main_loop()\n");

    SDL_RemoveTimer(fps_timer);
    scn_shutdown_manager();
    text_shutdown();
    TTF_Quit();

    return 0;
}

