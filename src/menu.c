#include "menu.h"

#include <assert.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "logging.h"
#include "memory.h"
#include "utils.h"

menu_t* mn_create(const char* font_name, SDL_Renderer* render, SDL_Color color, int ptsize) {
    menu_t* m = mem_calloc(1, sizeof(menu_t));

    assert(font_name);

    m->font= TTF_OpenFont(font_name, ptsize);

    int w, h;
    SDL_RenderGetLogicalSize(render, &w, &h);

    m->tmp_surf = SDL_CreateRGBSurface(0, w, h, 32, 0, 0, 0, 0);
    assert(m->tmp_surf);

    if (!m->font) {
        LOG("Ohh, TTF_OpenFont(%s, %d) failed with message %s\n", font_name, ptsize, TTF_GetError());
        LOG("Calling SDL_Quit()\n");
        SDL_Quit();
    }

    m->color = color;

    return m;
}

void mn_free(menu_t* m) {
    if (!m) {
        return;
    }

    TTF_CloseFont(m->font);
    SDL_FreeSurface(m->tmp_surf);

    menu_item_t* item = m->items_head;
    while (item) {
        if (item->text) {
            mem_free(item->text);
        }

        item = item->next;
    }

    mem_free(m);
}

menu_item_t* mn_item_add(menu_t* m, const char* text) {
    menu_item_t* new = mem_calloc(1, sizeof(menu_item_t));

    new->selected = false;
    new->next = NULL;

    mn_item_text_update(m, new, text);

    if (m->items_head) {
        menu_item_t* last = m->items_head;
        while (last->next) {
            last = last->next;
        }
        last->next = new;
    } else {
        m->items_head = new;
    }

    m->num++;
    return new;
}

menu_item_t* mn_item_find(menu_t* m, const char* text) {
    assert(m);
    menu_item_t* i = m->items_head;
    while (i) {
        if (!strcmp(text, i->text)) {
            return i;
        }
        i = i->next;
    }
    return NULL;
}

void mn_item_text_update(menu_t* m, menu_item_t* item, const char* text) {
    assert(item);
    assert(m);
    assert(text);

    if (item->text) {
        mem_free(item->text);
    }

    item->text = strdup(text);
    item->text_is_updated = true;

    item->surface = TTF_RenderUTF8_Blended(m->font, text, m->color);
    if (!item->surface) {
        LOG("mn_item_text_update :%s\n", TTF_GetError());
        SDL_Quit();
    }

}

bool mn_item_is_selected(menu_t* m, const char* text) {
    menu_item_t* t = mn_item_find(m, text);
    return t ? t->selected : false;
}

void mn_render(menu_t* m, SDL_Renderer* render) {
    assert(m);
    assert(render);

    int w, h;
    SDL_RenderGetLogicalSize(render, &w, &h);

    //Background
    surface_fill_solid(m->tmp_surf, map2SDL_Color(COLOR_GREEN));

    //First pass throw menu items. Blitting menu text string to temp surface
    int y = (h - m->items_head->surface->h * m->num) / 2;
    for (menu_item_t* i = m->items_head; i != NULL; i = i->next) {
        //Central text alignment
        int x = (w - i->surface->w) / 2;
        SDL_Rect src_r = make_rect(0, 0, i->surface->w, i->surface->h);
        SDL_Rect dst_r = make_rect(x, y, i->surface->w, i->surface->h);

        SDL_UpperBlit(i->surface, &src_r, m->tmp_surf, &dst_r);
        y += i->surface->h;
    }

    SDL_Texture* tmp_tex = SDL_CreateTextureFromSurface(render, m->tmp_surf);
    assert(tmp_tex);

    SDL_Rect d_rect = make_rect(0, 0, w, h);

    int copy_err = SDL_RenderCopy(render, tmp_tex, &d_rect, &d_rect);
    assert(copy_err == 0);

    //Second pass throw menu items for rendering selection rectangle
    y = (h - m->items_head->surface->h * m->num) / 2;
    for (menu_item_t* i = m->items_head; i != NULL; i = i->next) {
        if (i->selected) {
            color_t color = COLOR_BLACK;
            SDL_Rect bound = make_rect((w - i->surface->w) / 2, y, 
                    i->surface->w,
                    i->surface->h);
            SDL_SetRenderDrawColor(render, color.r, color.g, color.b, color.a);

            SDL_RenderDrawRect(render, &bound);
            bound = extend_rect(bound, 3);
            SDL_RenderDrawRect(render, &bound);
        }
        y += i->surface->h;
    }

    SDL_DestroyTexture(tmp_tex);
}

bool mn_update(menu_t* m, SDL_Event* event) {
    bool ret = false;

    if (event->type == SDL_KEYDOWN) {
        switch (event->key.keysym.sym) {
            case SDLK_ESCAPE:
                {
                    SDL_Event quit_ev;
                    quit_ev.type = SDL_QUIT;
                    SDL_PushEvent(&quit_ev);
                }
                break;
            case SDLK_UP: 
                {
                    menu_item_t* p = NULL;
                    for (menu_item_t* i = m->items_head; i != NULL; p = i, i = i->next) {
                        if (i->selected && p) { 
                            i->selected = false;
                            p->selected = true;
                            break;
                        }
                    }
                }
                break;
            case SDLK_DOWN:
                {
                    menu_item_t* n = m->items_head->next;
                    for (menu_item_t* i = m->items_head; i != NULL; i = i->next) {
                        if (i->selected && n) { 
                            i->selected = false;
                            n->selected = true;
                            break;
                        }

                        if (n && n->next) {
                            n = n->next;
                        }
                    }
                }
            break;
            case SDLK_RETURN:
            case SDLK_SPACE:
                {
                    ret = true;
                }
            break;
        }
    }

    return ret;
}

