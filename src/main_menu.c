﻿#include "main_menu.h"

#include <stdio.h>
#include <stdlib.h>

#include "logging.h"
#include "memory.h"
#include "scene.h"

#define MENU_FONT_SIZE  64
#define MENU_FONT_NAME  "DejaVuSansMono.ttf"

void sn_menu_init(scene_t* sn, SDL_Window* window) {
    LOG("sn_menu_init()\n");

    scn_cast(sn_main_menu_t*, st, sn);

    st->mn = mn_create(MENU_FONT_NAME, 
            SDL_GetRenderer(window),
            (SDL_Color){ .r  = 20, .g = 20, .b = 100, .a = 0},
            MENU_FONT_SIZE);

    mn_item_add(st->mn, "new");
    mn_item_add(st->mn, "train");
    mn_item_add(st->mn, "play")->selected = true;
    mn_item_add(st->mn, "reset");
    mn_item_add(st->mn, "about authors)");
    mn_item_add(st->mn, "quit");
}

void sn_menu_shutdown(scene_t* sn) {
    scn_cast(sn_main_menu_t*, st, sn);

    mn_free(st->mn);
}

void sn_menu_update(scene_t* sn) {
}

void sn_menu_render(scene_t* sn, SDL_Renderer* render) {
    scn_cast(sn_main_menu_t*, st, sn);
    mn_render(st->mn, render);
}

void sn_menu_poll(scene_t* sn, SDL_Event* ev) {
    scn_cast(sn_main_menu_t*, st, sn);

    if (mn_update(st->mn, ev)) {
        if (mn_item_find(st->mn, "play")->selected) {
            scn_set_active("nback");
        } else if (mn_item_find(st->mn, "quit")->selected) {
            SDL_Event quit_event;
            quit_event.type = SDL_QUIT;
            SDL_PushEvent(&quit_event);
        }
    }
}

void main_menu_init_scene(void) {
    scene_t* sn = scn_create("main_menu", mem_alloc(sizeof(sn_main_menu_t)), mem_free);

    sn->on_event = sn_menu_poll;
    sn->on_init = sn_menu_init;
    sn->on_render = sn_menu_render;
    sn->on_shutdown = sn_menu_shutdown;
    sn->on_update = sn_menu_update;
}
