﻿#include "scene.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

#define MAX_SCENES 64

static scene_t g_scenes[MAX_SCENES] = {};
scene_t* g_active_scene = NULL;
int g_scene_num = 0;

void scn_init_manager(SDL_Window* window) {
    for (int i = 0; i < g_scene_num; i++) {
        if (g_scenes[i].on_init) {
            g_scenes[i].on_init(&g_scenes[i], window);
        }
    }
}

void scn_shutdown_manager(void) {
    for (int i = 0; i < g_scene_num; i++) {
        if (g_scenes[i].on_shutdown) {
            g_scenes[i].on_shutdown(&g_scenes[i]);
        }
    }
}

scene_t* scn_create(const char* name, void* data, 
        void (*data_destructor)(void* ptr)) {
    assert(name);
    assert(g_scene_num + 1 < MAX_SCENES);

    scene_t* s = &g_scenes[g_scene_num++];

    memset(s, 0, sizeof(scene_t));
    strcpy(s->name, name);
    s->data = data;
    s->data_destructor = data_destructor;

    return s;
}

void scn_free(const char* name) {
    scene_t* s = scn_get(name);
    assert(s);

    if (s == g_active_scene) {
        g_active_scene = NULL;
    }

    //XXX Untested code
    while (s != (&g_scenes[g_scene_num])) {
        *s = *(s + 1);
        s++;
    }
    g_scene_num--;
    assert(g_scene_num >= 0); //Just in case
}

scene_t* scn_get(const char* name) {
    if (!name) {
        return NULL;
    }
    scene_t* s = g_scenes;

    for (int i = 0; i < g_scene_num; i++, s++) {
        if (!strcmp(name, s->name)) {
            return s;
        }
    }

    return NULL;
}

scene_t* scn_get_active() {
    return g_active_scene;
}

void scn_set_active(const char* name) {
    scene_t* new = scn_get(name);
    assert(new);

    if (g_active_scene && g_active_scene->on_disable) {
        g_active_scene->on_disable(g_active_scene);
    }

    if (new->on_enable) {
        new->on_enable(new);
    }

    g_active_scene = new;
}

void scn_update() {
    if (g_active_scene && g_active_scene->on_update) {
        g_active_scene->on_update(g_active_scene);
    }
}

void scn_poll(SDL_Event* ev) {
    if (g_active_scene && g_active_scene->on_event) {
        g_active_scene->on_event(g_active_scene, ev);
    }
}

void scn_render(SDL_Renderer* render) {
    if (g_active_scene && g_active_scene->on_render) {
        g_active_scene->on_render(g_active_scene, render);
    }
}

