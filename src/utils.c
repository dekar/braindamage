﻿#include "utils.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

static char rect_str_buf[128];

const char* rect2str(const SDL_Rect* rect) {
    assert(rect);
    int err = sprintf(rect_str_buf, "{ %d, %d, %d, %d}", 
            rect->x, rect->y, rect->w, rect->h);
    return err < 0 ? NULL : (const char*)rect_str_buf;
}

void surface_fill_solid(SDL_Surface* surf, SDL_Color color) {
    //TODO There is no call of SDL_FillRect() because of I suggest to add some pattern filling
    int is_locked = SDL_LockSurface(surf);
    assert(is_locked == 0);

    uint32_t c = map2color_t(color).c;
    uint32_t* pixels = surf->pixels;
    for (int iw = 0; iw < surf->w; iw++) {
        for (int ih = 0; ih < surf->h; ih++) {
            pixels[ih * surf->w + iw] = c;
        }
    }

    SDL_UnlockSurface(surf);
}

void make_screen_shot(SDL_Renderer* render) {
    //TODO increment snapshots file naming
    assert(render);
    int w, h;
    SDL_RenderGetLogicalSize(render, &w, &h);
    SDL_Surface *screen = SDL_CreateRGBSurface(0, w, h, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);
    SDL_RenderReadPixels(render, NULL, SDL_PIXELFORMAT_ARGB8888, screen->pixels, screen->pitch);
    SDL_SaveBMP(screen, "screenshot.bmp");
    SDL_FreeSurface(screen);
}
