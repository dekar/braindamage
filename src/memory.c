﻿#include "memory.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "logging.h"

void* mem_alloc(size_t size) {
    return malloc(size);
}

void* mem_calloc(size_t num, size_t size) {
    return calloc(num, size);
}

void* mem_realloc(void* ptr, size_t size) {
    return realloc(ptr, size);
}

bool check_ptr(void* ptr) {
    assert(0);
}

void mem_free(void* ptr) {
    free(ptr);
}

void mem_check_heap(void) {
    assert(0);
}

void mem_print_info(void) {
}

