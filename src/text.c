﻿#include "text.h"

#include <assert.h>
#include <stdarg.h>
#include <string.h>

#include "engine.h"

#define MAX_FONTS_COUNT 8

typedef struct font_t {
    TTF_Font*   font;
    char        name[32];
} font_t;

font_t      g_fonts[MAX_FONTS_COUNT];
uint32_t    g_fonts_count;
font_t*     g_active_font;

SDL_Colour      g_active_color;
text_align_t    g_alignment;

void text_load_font(char* fname, char* tagname, int ptsize) {
    assert(fname);
    assert(tagname);
    assert(g_fonts_count < MAX_FONTS_COUNT);

    font_t* f = &g_fonts[g_fonts_count++];

    f->font = TTF_OpenFont(fname, ptsize);

    if (!f->font) {
        LOG("Ohh, TTF_OpenFont(%s, %d) failed with message %s\n", f->name, ptsize, TTF_GetError());
        LOG("Calling SDL_Quit()\n");
        SDL_Quit();
    }

    assert(f->font);
    strcpy(f->name, tagname);
}

//FIXME Sloooow bullshit
void text_render(SDL_Renderer* render, SDL_Rect r, char* text) {
    assert(render);
    assert(text);
    assert(g_active_font);

    SDL_Surface* tmp_surf = TTF_RenderUTF8_Blended(g_active_font->font, text, g_active_color);
    assert(tmp_surf);

    SDL_Texture* tmp_tex = SDL_CreateTextureFromSurface(render, tmp_surf);
    assert(tmp_tex);

    int text_w;
    TTF_SizeText(g_active_font->font, text, &text_w, NULL);

    int w, h;
    SDL_QueryTexture(tmp_tex, NULL, NULL, &w, &h);
    SDL_Rect dst_rect;

    if (g_alignment & ALIGN_LEFT) {
        dst_rect.x = r.x;
        dst_rect.y = r.y;
        dst_rect.w = w;
        dst_rect.h = h;
    } 
    if (g_alignment & ALIGN_CENTER) {
        dst_rect.x = r.x + (r.w - text_w) / 2;
        dst_rect.y = r.y;
        dst_rect.w = w;
        dst_rect.h = h;
    }
    if (g_alignment & ALIGN_VERTICAL) {
        int text_h = TTF_FontHeight(g_active_font->font);
        dst_rect.y = r.y + (r.h - text_h) / 2;
    }

    SDL_RenderCopy(render, tmp_tex, NULL, &dst_rect);

    SDL_FreeSurface(tmp_surf);
    SDL_DestroyTexture(tmp_tex);
}

void text_renderf(SDL_Renderer* render, SDL_Rect r, char* text, ...) {
    char    buf[256];
    va_list args;
    
    va_start(args, text);

    int ret = vsnprintf(buf, sizeof(buf), text, args);
    assert((uint32_t)ret < sizeof(buf));
    
    va_end(args);

    text_render(render, r, buf);
}

TTF_Font* text_get_active_font(void) {
    assert(g_active_font);
    return g_active_font->font;
}

void text_set_active_font(char* fname) {
    assert(fname);

    for (uint32_t i = 0; i < g_fonts_count; i++) {
        if (strcmp(fname, g_fonts[i].name) == 0) {
            g_active_font = &g_fonts[i];
            return;
        }
    }

    LOG("text_set_active_font(): %s - not found!\n", fname);
    abort();
}

void text_set_color(SDL_Color color) {
    g_active_color = color;
}

void text_set_align(text_align_t a) {
    g_alignment = a;
}

void text_init(void) {
    g_fonts_count = 0;
    g_active_font = NULL;

    g_active_color.r = 0;
    g_active_color.g = 0;
    g_active_color.b = 0;
    g_active_color.a = 255; //FIXME What is this value is?

    g_alignment = ALIGN_LEFT;
}

void text_shutdown(void) {
    for (uint32_t i = 0; i < g_fonts_count; i++) {
        TTF_CloseFont(g_fonts[i].font);
    }
}

