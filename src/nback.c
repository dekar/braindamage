#include "nback.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "engine.h"

#define BOTTOM_TEXT_SIZE    16
#define UPPER_TEXT_SIZE     16
#define START_TEXT_SIZE     48
#define TEXT_ANIMATION_TIME 30
#define GAME_TICK_INTERVAL  10

#define COLOR_NEW_GAME (SDL_Color) { .r = 80, .g = 80, .b = 100, .a = 255 }
#define COLOR_UPPER_TEXT (SDL_Color) { .r = 20, .g = 20, .b = 50, .a = 255 }

typedef struct visual_info_t {
    int x0, y0;
    int w, h; 
} visual_info_t;

static void game_stop(scene_t* sn, nback_statistic_t* stat);

static void generate_signals(nback_signal_t* signals, int count, uint32_t mask) {
    assert(count <= NBACK_MAX_SIGNALS_COUNT);
    assert(signals);

    for (int i = 0; i < count; i++) {
        if (mask & NBACK_SIGNAL_POSITION) {
            signals[i].x = rand() % GRID_DIM;
            signals[i].y = rand() % GRID_DIM;
            LOG("signal %d - %d %d created\n", i, signals[i].x, signals[i].y);
        }
    }
}

static uint32_t game_tick_cb(uint32_t interval, void* param) {
    scn_cast(sn_nback_t*, st, param);

    LOG("game_tick_cb\n");
    if (st->text_animation_counter > 0) {
        st->text_animation_counter--;
    }

    return GAME_TICK_INTERVAL;
}

static uint32_t on_show_cb(uint32_t interval, void* param) {
    scn_cast(sn_nback_t*, st, param);

    if (st->current_signal > 0) {
        st->current_signal--;
    } else {
        game_stop(param, NULL);
    }

    return interval;
}

static void game_start(scene_t* sn) {
    scn_cast(sn_nback_t*, st, sn);

    st->current_signal = st->signals_count;
    //TODO variable 3rd generate_signals parameter
    generate_signals(st->signals, st->signals_count, NBACK_SIGNAL_POSITION);

    st->show_timer = SDL_AddTimer(st->def.wait_time, on_show_cb, sn);
    st->game_timer = SDL_AddTimer(GAME_TICK_INTERVAL, game_tick_cb, sn);
}

static void game_stop(scene_t* sn, nback_statistic_t* stat) {
    scn_cast(sn_nback_t*, st, sn);

    SDL_RemoveTimer(st->show_timer);
    
    //TODO calculate statistic
}

//TODO Implement here: state->def = def;
void sn_nback_init(scene_t* sn, SDL_Window* window) {
    LOG("sn_nback_init()\n");

    scn_cast(sn_nback_t*, st, sn);

    nback_def_t def;
    def.signal_mask |= NBACK_SIGNAL_POSITION;
    def.wait_time = NBACK_WAIT_TIME;

    st->def = def;
    st->back_level = 1;
    st->signals_count = 10;
    st->is_running = false;

    text_load_font("dejavu.ttf", "bottom_text", BOTTOM_TEXT_SIZE);
    text_load_font("dejavu.ttf", "upper_text", UPPER_TEXT_SIZE);
    text_load_font("dejavu.ttf", "start_text", START_TEXT_SIZE);
}

void sn_nback_shutdown(scene_t* sn) {
    sn_nback_t* st = (sn_nback_t*)sn->data;

    SDL_RemoveTimer(st->show_timer);
    SDL_RemoveTimer(st->game_timer);
}

void sn_nback_update(scene_t* sn) {
}

static inline visual_info_t get_visual_info(SDL_Renderer* render) {
    visual_info_t ret;
    SDL_RenderGetLogicalSize(render, &ret.w, &ret.h);

    ret.x0 = (ret.w - CELL_SIZE * GRID_DIM) / 2;
    ret.y0 = (ret.h - CELL_SIZE * GRID_DIM) / 2;

    return ret;
}

static void render_quad(SDL_Renderer* render, int cellx, int celly) {
    const int border = 2;
    visual_info_t inf = get_visual_info(render);
    SDL_Rect r = make_rect(
            inf.x0 + cellx * CELL_SIZE + border, 
            inf.y0 + celly * CELL_SIZE + border,
            CELL_SIZE - border * 2, 
            CELL_SIZE - border * 2);
    set_render_color(render, COLOR_BLACK);
    SDL_RenderFillRect(render, &r);
}

static void render_lines(SDL_Renderer* render, int x0, int y0, int x1, int y1, 
        int dx, int dy, int count) {
    for (int i = 0; i <= count; i++) {
        SDL_RenderDrawLine(render, x0 + dx * i, y0 + dy * i, x1 + dx * i, y1 + dy * i); 
    }
}

//TODO No statistic write yet..
void render_statistic(SDL_Renderer* render, SDL_Rect bound) {
    SDL_Rect rect;

    text_set_active_font("dejavu24");

    int h = TTF_FontLineSkip(text_get_active_font());

    text_set_align(ALIGN_CENTER);
    rect = bound;
    text_renderf(render, rect, "bla-bla");
    rect.y += h;
    text_renderf(render, rect, "bu-bu-be");
    rect.y += h;
    text_renderf(render, rect, "la-go-po");
    rect.y += h;
    text_renderf(render, rect, "точность");
    rect.y += h;
    text_renderf(render, rect, "тарабар");
}

void sn_nback_render(scene_t* sn, SDL_Renderer* render) {
    scn_cast(sn_nback_t*, st, sn);

    //drawing active quad
    nback_signal_t* sig = &st->signals[st->current_signal];
    render_quad(render, sig->x, sig->y);
    //end of drawing quad

    int w, h;
    SDL_RenderGetLogicalSize(render, &w, &h);

    int x0 = (w - CELL_SIZE * GRID_DIM) / 2;
    int y0 = (h - CELL_SIZE * GRID_DIM) / 2;
    SDL_Rect grid_rect = make_rect(x0, y0, CELL_SIZE * GRID_DIM, CELL_SIZE * GRID_DIM);

    set_render_color(render, COLOR_BLUE);

    //drawing grid
    render_lines(render, x0, y0, 
            x0 + CELL_SIZE * GRID_DIM, 
            y0,
            0, CELL_SIZE, 
            GRID_DIM);
    render_lines(render, x0, y0, 
            x0, 
            y0 + CELL_SIZE * GRID_DIM,
            CELL_SIZE, 0, 
            GRID_DIM);
    //end of drawing grid

    //drawing staistic information
    render_statistic(render, grid_rect);
    //end of drawing staistic information

    //drawing upper text
    SDL_Color upper_text_color = COLOR_UPPER_TEXT;
    text_set_color(upper_text_color);
    text_set_active_font("upper_text");
    text_set_align(ALIGN_CENTER);
    text_renderf(render, 
            make_rect(x0, y0 - TTF_FontLineSkip(text_get_active_font()), grid_rect.w, grid_rect.h), 
            "%d / %d", 
            st->signals_count - st->current_signal,
            st->signals_count);
    //end of drawing upper texts

    //drawing bottom texts
    SDL_Color bottom_text_color;
    if (st->text_animation_counter > 0) {
        bottom_text_color = map2SDL_Color(COLOR_RED);
    } else {
        bottom_text_color = map2SDL_Color(COLOR_BLUE);
    }
    text_set_color(bottom_text_color);
    text_set_active_font("bottom_text");
    text_set_align(ALIGN_LEFT);
    text_render(render, make_rect(x0, y0 + CELL_SIZE * GRID_DIM, 0, 0), "A: позиция");
    //end of drawing bottom texts

    //drawing "Press Space" title
    if (!st->is_running) {
        SDL_Rect start_text_rect = grid_rect;
        text_set_align(ALIGN_CENTER | ALIGN_VERTICAL);
        text_set_color(COLOR_NEW_GAME);
        text_set_active_font("start_text");
        text_render(render, start_text_rect, "Press Space to start");
    }
    //end of drawing
}

static void sn_nback_space_press(scene_t* sn) {
    scn_cast(sn_nback_t*, st, sn);

    if (st->is_running) {
        LOG("call game_stop()\n");
        game_stop(sn, NULL);
    } else {
        LOG("call game_start()\n");
        game_start(sn);
    }

    st->is_running = !st->is_running;
}

void sn_nback_position_press(scene_t* sn) {
    scn_cast(sn_nback_t*, st, sn);

    if (st->is_running) {
        st->text_animation_counter = TEXT_ANIMATION_TIME;

        int check_index = st->current_signal - st->back_level;

        if (check_index >= 0 && 
            st->signals[check_index].x == st->signals[st->current_signal].x &&
            st->signals[check_index].y == st->signals[st->current_signal].y) {
            LOG("yep\n");
        }
    }
}

void sn_nback_poll(scene_t* sn, SDL_Event* ev) {
    scn_cast(sn_nback_t*, st, sn);

    if (ev->type == SDL_KEYDOWN) {
        switch (ev->key.keysym.sym) {
            case SDLK_ESCAPE: 
                    scn_set_active("main_menu");
                break;
            case SDLK_SPACE:
                    sn_nback_space_press(sn);
                break;
            case SDLK_a:
                    sn_nback_position_press(sn);
                break;
        }
    }
}

void nback_init_scene(void) {
    scene_t* sn = scn_create("nback", mem_alloc(sizeof(sn_nback_t)), mem_free);

    sn->on_event = sn_nback_poll;
    sn->on_init = sn_nback_init;
    sn->on_render = sn_nback_render;
    sn->on_shutdown = sn_nback_shutdown;
    sn->on_update = sn_nback_update;
}
