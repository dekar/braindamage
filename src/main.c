#include "engine.h"

#include "main_menu.h"
#include "nback.h"

void entryPoint(void) {
    main_menu_init_scene();
    nback_init_scene();
    scn_set_active("main_menu");
}

int main(int argc, char* argv[]) {
    int err = main_loop(entryPoint);
    return err;
}
