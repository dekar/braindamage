#pragma once

#include <stdbool.h>

#include <SDL2/SDL.h>

#include "menu.h"

typedef struct sn_main_menu_t {
    menu_t* mn;
} sn_main_menu_t;

void main_menu_init_scene(void);
