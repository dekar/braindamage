﻿#pragma once

#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "app.h"
#include "logging.h"
#include "memory.h"
#include "menu.h"
#include "scene.h"
#include "text.h"
#include "utils.h"
