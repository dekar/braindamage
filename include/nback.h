#pragma once

#include <stdbool.h>

#include <SDL2/SDL.h>

/**
 * Game field is quad with size of GRID_DIM * GRID_DIM
 */
#define GRID_DIM 3
/**
 * Size of one game filed quad.
 */
#define CELL_SIZE 80
/**
 * Pause in msec between signals.
 */
#define NBACK_MAX_LEVEL         10
#define NBACK_MAX_SIGNALS_COUNT 50
#define NBACK_WAIT_TIME         1000

typedef enum {
    NBACK_SIGNAL_POSITION   = 0x00000001,
    NBACK_SIGNAL_SOUND      = 0x00000002
} nback_signal_flags;

typedef struct nback_def_t {
    uint32_t signal_mask; ///Supported ::nback_signal_flags
    uint32_t wait_time;
} nback_def_t;

typedef struct nback_signal_t {
    int x, y;       //Position coordinates 0 <= GRID_DIM
} nback_signal_t;

typedef struct nback_statistic_t {
    float nback_max;
    float nback_avg;
    uint32_t set_time; ///Time of one set. Pause time * count of signals
} nback_statistic_t;

typedef struct sn_nback_t {
    nback_def_t     def;
    bool            is_running;     ///Is game cycle run(start by Space button)
    int32_t         remain_time;    ///Time in milliseconds to end of train set
    SDL_TimerID     show_timer;
    SDL_TimerID     game_timer;
    nback_signal_t  signals[NBACK_MAX_SIGNALS_COUNT];
    uint32_t        back_level; ///N level of back
    uint32_t        current_signal;
    uint32_t        signals_count;
    uint32_t        text_animation_counter;
} sn_nback_t;

void nback_init_scene(void);
