﻿#pragma once

#include <stdint.h>

//There is header file confusion between mingw(windows platform)
//and Linux(posix platform)
#if defined(BD_WIN)
#include <SDL2/SDL_ttf.h>
#else
#include <SDL/SDL_ttf.h>
#endif

#include <SDL2/SDL.h>

typedef union {
    struct __attribute__ ((__packed__)) {
        uint8_t b;
        uint8_t g;
        uint8_t r;
        uint8_t a;
    };
    uint32_t c;
} color_t;

#define COLOR_BLACK  ((color_t){ .r = 0, .g = 0, .b = 0, .a = 255})
#define COLOR_WHITE  ((color_t){ .r = 255, .g = 255, .b = 255, .a = 255})
#define COLOR_RED    ((color_t){ .r = 255, .g = 0, .b = 0, .a = 255})
#define COLOR_GREEN  ((color_t){ .r = 0, .g = 255, .b = 0, .a = 255})
#define COLOR_BLUE   ((color_t){ .r = 0, .g = 0, .b = 255, .a = 255})

const char* rect2str(const SDL_Rect* rect);
void surface_fill_solid(SDL_Surface* surf, SDL_Color color);
void make_screen_shot(SDL_Renderer* render);

static inline color_t map2color_t(SDL_Color c) {
    return (color_t){ .r = c.r, .g = c.g, .b = c.b, .a = c.a };
}

static inline SDL_Color map2SDL_Color(color_t c) {
    return (SDL_Color){ .r = c.r, .g = c.g, .b = c.b, .a = c.a };
}

static inline SDL_Rect make_rect(int x, int y, int w, int h) {
    return (SDL_Rect){ .x = x, .y = y, .w = w, .h = h};
}

static inline SDL_Rect extend_rect(SDL_Rect rect, int s) {
    return (SDL_Rect){ .x = rect.x - s, .y = rect.y - s, .w = rect.w + s * 2, .h = rect.h + s * 2};
}

static inline void set_render_color(SDL_Renderer* render, color_t c) {
    SDL_SetRenderDrawColor(render, c.r, c.g, c.b, c.a);
}
