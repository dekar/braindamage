﻿#pragma once

#include <SDL2/SDL.h>

#include "utils.h"

typedef enum text_align_t {
    ALIGN_LEFT = 0x01,
    ALIGN_CENTER = 0x02,
    ALIGN_VERTICAL = 0x4
} text_align_t;

void text_load_font(char* fname, char* tagname, int ptsize);
void text_render(SDL_Renderer* render, SDL_Rect r, char* text);
void text_renderf(SDL_Renderer* render, SDL_Rect r, char* text, ...);
void text_set_active_font(char* fname);
TTF_Font* text_get_active_font(void);
void text_set_color(SDL_Color color);
void text_set_align(text_align_t a);

void text_init(void);
void text_shutdown(void);
