﻿#pragma once

#include <stdio.h>

void*   mem_alloc(size_t size);
void*   mem_calloc(size_t num, size_t size);
void*   mem_realloc(void* ptr, size_t size);
void    mem_free(void* ptr);
void    mem_check_heap(void);
void    mem_print_info(void);
