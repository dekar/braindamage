#pragma once

#include <assert.h>

#include <SDL2/SDL.h>

/**
 * Represent scene information - name, user data and callbacks functuons.
 */
typedef struct scene_t {
    void (*on_update)(struct scene_t* scn);
    void (*on_render)(struct scene_t* scn, SDL_Renderer* render);
    void (*on_event)(struct scene_t* scn, SDL_Event* ev);

    void (*on_enable)(struct scene_t* scn);
    void (*on_disable)(struct scene_t* scn);

    void (*on_init)(struct scene_t* scn, SDL_Window* window);
    void (*on_shutdown)(struct scene_t* scn);

    char name[32];
    void (*data_destructor)(void* ptr);
    void* data;
} scene_t;

#ifdef __GNUC__
#define VARIABLE_IS_NOT_USED __attribute__ ((unused))
#else
#define VARIABLE_IS_NOT_USED
#endif

static inline scene_t* scn_check(struct scene_t* scn)  {
    assert(scn);
    return scn;
};

#define scn_cast(type, variable, scene) \
    type VARIABLE_IS_NOT_USED variable = (type)(scn_check(scene)->data);

/**
 * Call on_init() function of all created scenes. 
 * It called in main_menu() _after_ entryPoint().
 * @param  window
 */
void scn_init_manager(SDL_Window* window);
/**
 * Call on_shutdown() function of all scenes.
 */
void scn_shutdown_manager(void);

scene_t* scn_create(const char* name, void* data, void (*data_destructor)(void* ptr));
void scn_free(const char* name);

scene_t* scn_get(const char* name);

scene_t* scn_get_active();
void scn_set_active(const char* name);

void scn_update();
void scn_render(SDL_Renderer* render);
void scn_poll(SDL_Event* ev);

