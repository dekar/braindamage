﻿#pragma once

#include <SDL2/SDL.h>

typedef struct fps_counter_t {
    uint32_t counter;
    uint32_t value;
} fps_counter_t;

extern fps_counter_t g_fps;

int main_loop(void (*entryPoint)(void));

