#pragma once

#include <stdbool.h>

#include <SDL2/SDL.h>

//There is header file location confusion between mingw(windows platform)
//and Linux(posix platform)
#if defined(BD_WIN)
#include <SDL2/SDL_ttf.h>
#else
#include <SDL/SDL_ttf.h>
#endif

typedef struct menu_item_t {
    char* text;
    SDL_Surface* surface;
    bool text_is_updated;
    bool selected;
    struct menu_item_t *next;
} menu_item_t;

typedef struct {
    menu_item_t* items_head;
    int num; ///Number of menu items
    TTF_Font*    font;
    SDL_Surface* tmp_surf;
    SDL_Color    color;
} menu_t;

menu_t* mn_create(const char* font_name, SDL_Renderer* render, SDL_Color color, int ptsize);
void mn_free(menu_t* m);

menu_item_t* mn_item_add(menu_t* m, const char* text);
menu_item_t* mn_item_find(menu_t* m, const char* text);
void mn_item_text_update(menu_t* m, menu_item_t* item, const char* text);
bool mn_item_is_selected(menu_t* m, const char* text);

void mn_render(menu_t* m, SDL_Renderer* render);
//Return true if selected item pushed
bool mn_update(menu_t* m, SDL_Event* event);
