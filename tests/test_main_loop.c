#include "app.h"
#include "main_menu.h"
#include "nback.h"
#include "scene.h"

void entryPoint() {
    scn_create("null", NULL, NULL);
    scn_set_active("null");
}

int main(int argc, char* argv[]) {
    int err = main_loop(entryPoint);
    return err;
}
