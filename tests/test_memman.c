#include <stdlib.h>
#include <time.h>

#include "logging.h"
#include "memory.h"

#define COUNT 20

int main(int argc, char* argv[]) {

    srand(time(0));

    void* pointers[COUNT];

    LOG("allocating %d times\n", COUNT);

    for (int i = 0; i < COUNT; i++) {
        pointers[i] = mem_alloc(rand() % 200);
    }

    LOG("freeing now..\n");

    for (int i = 0; i < COUNT; i++) {
        mem_free(pointers[i]);
    }

    LOG("OK\n");

    return 0;
}
