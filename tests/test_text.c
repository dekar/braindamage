#include "engine.h"

typedef struct sn_main_t {
    SDL_TimerID fps_timer;
} sn_main_t;

uint32_t fps_timer_callback(uint32_t interval, void* p) {
    LOG("FPS %d\n", g_fps.value);
    return 1000;
}

void main_on_init(scene_t* sn, SDL_Window* window) {
    scn_cast(sn_main_t*, s, sn);

    s->fps_timer = SDL_AddTimer(1000, fps_timer_callback, s);

    text_load_font("dejavu.ttf", "dejavu24", 24);
    text_set_active_font("dejavu24");
}

void main_on_shutdown(scene_t* sn) {
    scn_cast(sn_main_t*, s, sn);

    SDL_RemoveTimer(s->fps_timer);
}

void main_on_render(scene_t* sn, SDL_Renderer* render) {
    int w, h;
    SDL_RenderGetLogicalSize(render, &w, &h);
    for (int i = 0; i < 100; i++) {
        text_render(render, rand() % w, rand() % h, "Hello!");
    }
}

void entryPoint() {
    scene_t* s = scn_create("main", mem_alloc(sizeof(sn_main_t)), mem_free);
    s->on_init = main_on_init;
    s->on_shutdown = main_on_shutdown;
    s->on_render = main_on_render;

    scn_set_active("main");
}

int main(int argc, char* argv[]) {
    int err = main_loop(entryPoint);
    return err;
}
