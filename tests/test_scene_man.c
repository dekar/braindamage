#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <SDL2/SDL.h>

//There is header file confusion between mingw(windows platform)
//and Linux(posix platform)
#if defined(BD_WIN)
#include <SDL2/SDL_ttf.h>
#else
#include <SDL/SDL_ttf.h>
#endif

#include "logging.h"
#include "menu.h"
#include "nback.h"
#include "scene.h"
#include "utils.h"

void sn_enable(scene_t* scn) {
    assert(scn);
    LOG("sn_enable %s\n", scn->name);
}

void sn_disable(scene_t* scn) {
    assert(scn);
    LOG("sn_disable %s\n", scn->name);
}

void sn_render(scene_t* scn, SDL_Renderer* render) {
    assert(scn);
    LOG("sn_render %s\n", scn->name);
}

void sn_update(scene_t* scn) {
    assert(scn);
    LOG("sn_update %s\n", scn->name);
}

#define SET_HANDLERS(sn) \
    sn->on_enable = sn_enable;  \
    sn->on_enable = sn_enable;  \
    sn->on_render = sn_render;  \
    sn->on_update = sn_update;  \

void activate_and_check(const char* name) {
    LOG("call scn_set_active(%s)\n", name);
    scn_set_active(name);

    LOG("update call\n");
    scn_update(NULL);
    LOG("update OK\n");

    LOG("render call\n");
    scn_render(NULL);
    LOG("render OK\n");
}

int main(int argc, char* argv[]) {
    scn_init_manager(NULL);

    scene_t* menu_sn = scn_create("menu", NULL, NULL);
    scene_t* nback_sn = scn_create("nback", NULL, NULL);
    scene_t* help_sn = scn_create("help", NULL, NULL);
    scene_t* welcome_animation_sn = scn_create("welcome_animation", NULL, NULL);

    LOG("scenes created\n");

    SET_HANDLERS(menu_sn);
    SET_HANDLERS(nback_sn);
    SET_HANDLERS(help_sn);
    SET_HANDLERS(welcome_animation_sn);

    activate_and_check("welcome_animation");
    activate_and_check("nback");
    activate_and_check("menu");
    activate_and_check("nback");

    scn_free("menu");
    activate_and_check("help");

    scn_shutdown_manager();

    return 0;
}
